#!/usr/bin/env python
## Copyright (c) 2021, Alliance for Open Media. All rights reserved
##
## This source code is subject to the terms of the BSD 3-Clause Clear License and the
## Alliance for Open Media Patent License 1.0. If the BSD 3-Clause Clear License was
## not distributed with this source code in the LICENSE file, you can obtain it
## at aomedia.org/license/software-license/bsd-3-c-c/.  If the Alliance for Open Media Patent
## License 1.0 was not distributed with this source code in the PATENTS file, you
## can obtain it at aomedia.org/license/patent-license/.
##
__author__ = "maggie.sun@intel.com, ryanlei@meta.com"

"""
Python file for definition of AV2 CTC testing clips/sets
"""

AV2_SUBJECTIVE_TEST = {
    "RA": ["A1"],
}

SUBJECTIVE_CLIPS = {
"A1"    :  ["BarScene_1920x1080_60fps_10bit_420.y4m",
            "GregoryCactus_fr216_515_1080x1920_30p_420_10b_SDR.y4m",
            "GregoryFence_fr0_299_1080x1920_30p_420_10b_SDR.y4m",
            "Marathon2_3840x2160_30fps_10bit_420pf.y4m",
            "meridian_aom_sdr_11872-12263.y4m",
            "Metro_1920x1080_60fps_10bit_420.y4m",
            "MountainBay2_3840x2160_30fps_420_10bit.y4m",
            "TallBuildings2_3840x2160_30fps_10bit_420pf.y4m",
            "YonseiS01_R_00_00.y4m"
            ]
}